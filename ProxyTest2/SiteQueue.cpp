#include "SiteQueue.h"
/********************************************************
* SiteQueue.cpp
* 
*/

SiteQueue::SiteQueue( )
{
	
}


SiteQueue::~SiteQueue()
{
}


void SiteQueue::fillQueue( const vector< string > &sites )
{
	for ( unsigned int i = 0; i < sites.size(); i++ )
	{
		websiteQueue.push( sites.at( i ) );
	}
}

string SiteQueue::getNextSite()
{
	string site = "";
	if ( !websiteQueue.empty() )
	{
		site = websiteQueue.front();
		websiteQueue.pop();
	}
	return site;
}

bool SiteQueue::empty()
{
	return websiteQueue.empty();
}