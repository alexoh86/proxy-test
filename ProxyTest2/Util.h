#pragma once

#include <fstream>
using std::ifstream;
using std::ofstream;
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <Windows.h>
#include <WinInet.h>
#include <string>
using std::string;
/********************************************************
* Util.h
* Prototypes for utility functions and useful
* data structures.
*/

extern ofstream logFile;

/*
ParseSites
	Parse the file that contains the websites.
	file - handle to input file stream
	sites - vector to store list of websites
*/
void ParseSiteFile( ifstream &file, vector< string > *sites );

string getDateTime();