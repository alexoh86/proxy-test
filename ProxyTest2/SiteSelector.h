#pragma once
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <Windows.h>
#include <WinInet.h>
/********************************************************
* SiteSelector.h
* Parses html code for href attributes and randomly 
* selects one of the sites.
*/
class SiteSelector
{
public:
	SiteSelector( string domain );
	~SiteSelector();

	DWORD readHTML( HINTERNET *hRequest );
	string getCandidateSite();
	BOOL sitesAvailable();
	int candidateSiteCount() const;
	const vector< string > & getAllSites() const ;
	void clearSites();


private:
	void saveHrefs();
	bool isGoodURL( string url );

	string htmlCode;
	string domain;
	vector< string > candidateSites;
	vector< string > exclude;
	CRITICAL_SECTION outputCS;
};

