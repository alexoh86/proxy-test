#pragma once

#include <windows.h>
#include <WinInet.h>
#include <string>

#include "SiteQueue.h"
#include "SiteSelector.h"

using std::string;

/*
	Data for thread ProxyTestThread()
*/
struct ThreadInfo
{
	string siteName;
	int threadID;
};

// globals
extern CRITICAL_SECTION outputCS;


// function prototypes
DWORD WINAPI ProxyTestThread( LPVOID lpParam );
DWORD getHttpStatusCode( HINTERNET &hOpenRequest, const string &siteName, const int threadID );
HINTERNET initSiteQueue( SiteQueue *queue, SiteSelector *selector, const ThreadInfo &tInfo );
void CS_Print( string message );
void CS_PrintErr( string message );
void CS_PrintF( string message );