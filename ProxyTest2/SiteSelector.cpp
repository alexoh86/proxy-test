#include <iostream>
using std::cout;
using std::endl;
#include <time.h>
#include "SiteSelector.h"
/********************************************************
* SiteSelector.cpp
* Parses html code for href attributes and randomly 
* selects one of the sites.
*/

SiteSelector::SiteSelector( string domainName )
{
	htmlCode = "";
	domain = domainName;
	exclude.push_back( ".css" );
	exclude.push_back( ".png" );
	exclude.push_back( ".ico" );
}


SiteSelector::~SiteSelector(void)
{
}

/********************************************************
	ReadHTML
	Reads Html returned by HttpSendRequest. Returns
	number of bytes read.
*/
DWORD SiteSelector::readHTML( HINTERNET *hRequest )
{
	const DWORD BYTES_TO_READ = 512;
	char lpReadBuff[ BYTES_TO_READ ] = { 0 };
	bool bAllDone = FALSE;
	DWORD bytesRead = 0;
	DWORD totalBytesRead = 0;

	do
	{
		if ( !InternetReadFile( *hRequest, &lpReadBuff, BYTES_TO_READ - 1, &bytesRead ) )
		{
			if ( GetLastError() == ERROR_INSUFFICIENT_BUFFER )
			{
				  cout << "Insufficient buffer\n" << endl;
			}
			else
            {
                cout << "InternetReadFileEx failed, error " << GetLastError() << '\n';
                cout.flush();
                return 0;
            }
		}

		if ( bytesRead > 0 )
		{
			if ( bytesRead == BYTES_TO_READ - 1 )
				lpReadBuff[ BYTES_TO_READ - 1 ] = '\0';
			else
				lpReadBuff[ bytesRead + 1 ] = '\0';
		}

		totalBytesRead += bytesRead;
		htmlCode.append( lpReadBuff );

		if ( bytesRead == 0 )
			bAllDone = TRUE;

	} while ( bAllDone == FALSE );

	saveHrefs();

	return totalBytesRead;
}

/********************************************************
	GetHrefs
	Parses HTML for href links and saves candidate sites.
*/
void SiteSelector::saveHrefs()
{
	string sub = "href=\"http://"; // sub is the substring to search for

	vector<size_t> positions; // holds all the positions that sub occurs within str

	// get the position of the href links
	size_t pos = htmlCode.find( sub, 0 );
	while( pos != string::npos )
	{
		positions.push_back( pos );
		pos = htmlCode.find( sub, pos+1 );
	}

	const int LINK_ADVANCE = 6; // amount to advance to end of href="http://

	// save the links
	for ( unsigned int i = 0; i < positions.size(); i++ )
	{
		size_t startPos = positions.at( i ) + LINK_ADVANCE;
		size_t endPos   = htmlCode.find_first_of( "\"", startPos );
		size_t subStrLength = endPos - startPos;
		string hrefLink = htmlCode.substr( startPos, subStrLength );

		if ( isGoodURL( hrefLink ) )
			candidateSites.push_back( hrefLink );
		else
			continue;
	}
}

/********************************************************
	getCandidateSite
	Get a random link out of the list of candidate sites.
*/
string SiteSelector::getCandidateSite()
{
	srand( ( unsigned int )time( NULL ) );
	if ( candidateSites.size() > 0 )
	{
		size_t index = rand() % candidateSites.size();
		return candidateSites.at( index );
	}
	return "NULL";
}

/********************************************************
	sitesAvailable
	Check to see if there are candidate sites available.
*/
BOOL SiteSelector::sitesAvailable()
{
	if ( candidateSites.size() == 0 )
		return false;

	return true;
}

/********************************************************
	clearSites
	Clear all available sites. Should be called after 
	calling getCandidateSite()
*/
void SiteSelector::clearSites()
{
	htmlCode = "";
	candidateSites.clear();
}

/********************************************************
	isGoodURL
	Makes sure an href link is valid to save as a 
	candidate site.
*/
bool SiteSelector::isGoodURL( string URL )
{
	bool goodURL = true;

	if ( URL.find( domain ) != string::npos )
		goodURL = false;
	
	for ( unsigned int i = 0; i < exclude.size(); i++ )
	{
		if ( URL.rfind( exclude.at( i ) ) != string::npos )
		{
			goodURL = false;
			break;
		}
	}

	return goodURL;
}

/********************************************************
	getAllSites
*/
const vector< string > & SiteSelector::getAllSites() const
{
	return candidateSites;
}


int SiteSelector::candidateSiteCount() const 
{
	return candidateSites.size();
}