#pragma once

#include <string>
using std::string;
#include <vector>
using std::vector;
#include <queue>
using std::queue;

/********************************************************
* SiteQueue.h
* Queues up websites to call when a site trail has gone
* dead.
*/

class SiteQueue
{
public:
	SiteQueue();
	~SiteQueue();

	void fillQueue( const vector< string > &sites );
	string getNextSite();
	bool empty();

private:
	queue< string > websiteQueue;
};