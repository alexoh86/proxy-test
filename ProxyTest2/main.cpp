#include <windows.h>
#include <iostream>
#include <vector>
#include <WinInet.h>
#include <fstream>

#include "Util.h"
#include "ProxyThread.h"

using std::vector;
using std::string;
using std::cout;
using std::endl;
using std::cin;
using std::istream;

ofstream logFile;

/********************************************************
	Main
		Entry point of program.

*/
int main( int argc, char **argv )
{
	int numThreads = 0;

	if ( argc == 2 )
		numThreads = atoi( argv[ 1 ] );
	else if ( argc > 2 )
	{
		cout << "Usage: ProxyTest2.exe [ number of threads ]\n";
		return 0;
	}
	else
	{
		cout << "Number of threads to create: ";
		cin >> numThreads;
	}

	if ( !( numThreads > 0 && numThreads < MAXIMUM_WAIT_OBJECTS ) )
	{
		cout << "Number of threads out of range.\n";
		return 0;
	}

	logFile.open( "output.txt" );

	if ( !logFile.is_open() )
	{
		cout << "Error, log file could not be created" << endl;
		return 0;
	}

	ifstream file;
	file.open( "sites.txt" );

	if ( !file.is_open() )
	{
		cout << "Error, file could not be opened" << endl;
		return 0;
	}

	if ( !InitializeCriticalSectionAndSpinCount( &outputCS, 0x00000400 ) ) 
        return 0;

	vector< string > sites;
	ParseSiteFile( file, &sites );
	file.close();

	HANDLE *threadHandles = new HANDLE[ numThreads ];		
	ThreadInfo *pThreadData = new ThreadInfo[ numThreads ];

	// Run proxy test threads. Each thread will open up a site,
	// gather links from that site, and randomly select a link to open
	for ( int i = 0; i < numThreads; i++ )
	{
		pThreadData[ i ].siteName = sites.at( i );
		pThreadData[ i ].threadID = i;

		threadHandles[ i ] = CreateThread( NULL, 0, ProxyTestThread, &pThreadData[ i ], 0, NULL );
	}

	WaitForMultipleObjects( numThreads, threadHandles, true, INFINITE );

	delete [] threadHandles;
	delete [] pThreadData;
	DeleteCriticalSection( &outputCS );
	return 0;
}