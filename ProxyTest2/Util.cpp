#include <iostream>
using std::cout;
using std::endl;
#include <sstream>
#include <ctime>

#include "Util.h"
/********************************************************
* Util.cpp
* Utility function definitions.
*/

/********************************************************
	ParseSiteFile
*/
void ParseSiteFile( ifstream &file, vector< string > *sites )
{
	if ( file.is_open() )
	{
		string line;
		string http = "http://";
		string url = "";
		// parse the file
		while( file.good() )
		{
			getline( file, line );
			url = http + line;
			sites->push_back( url );
		}
	}
	else
	{
		cout << "Error: Unable to open sites.txt\n";
		return;
	}
}

/********************************************************
	PrintDateTime
	Timestamp for log file.
*/
string getDateTime()
{
	time_t t = time( 0 );
	struct tm now;
	localtime_s( &now, &t );
	std::ostringstream stream;

	stream << ( now.tm_mon + 1 ) << '-'
		 << now.tm_mday << '-'
		 << ( now.tm_year + 1900 )
		 << ", " 
		 << now.tm_hour << ':'
		 << now.tm_min << " ";

	return stream.str();
}