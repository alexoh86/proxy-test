#include <sstream>
#include <iostream>
#include <vector>
#include <fstream>

#include "ProxyThread.h"
#include "SiteSelector.h"
#include "Util.h"

using std::vector;
using std::string;
using std::cout;
using std::endl;
using std::cin;
using std::istream;
using std::ofstream;

CRITICAL_SECTION outputCS;

/********************************************************
	ProxyTestThread
		
*/
DWORD WINAPI ProxyTestThread( LPVOID lpParam )
{
	ThreadInfo *threadData = ( ThreadInfo * )lpParam;
	SiteQueue siteQ;
	SiteSelector selector( threadData->siteName );

	HINTERNET hInstance = NULL;

	hInstance = initSiteQueue( &siteQ, &selector, *threadData );

	string website = selector.getCandidateSite();
	selector.clearSites();

	while( !siteQ.empty() )
	{
		HINTERNET hConnect = InternetOpenUrl( hInstance, website.c_str(), NULL, 0, 0, NULL );

		if ( NULL == hConnect )
		{
			CS_Print( website.c_str() );
			CS_PrintErr( "Error, InternetOpenUrl failed. Selecting new site from queue..." );
			CS_PrintF( website.c_str() );
			CS_PrintF( "Error, InternetOpenUrl failed. Selecting new site from queue...\n" );
			website = siteQ.getNextSite();
			selector.clearSites();
			continue;
			//return NULL;
		}

		getHttpStatusCode( hConnect, website.c_str(), threadData->threadID );

		// read HTML
		selector.readHTML( &hConnect );

		if ( selector.sitesAvailable() && selector.candidateSiteCount() > 10 )
			website = selector.getCandidateSite();
		else
			website = siteQ.getNextSite();

		selector.clearSites();

		InternetCloseHandle( hConnect );
	}
	
	InternetCloseHandle( hInstance );
	std::ostringstream tid;
	tid << threadData->threadID;
	CS_Print( "Thread " + tid.str() + " has completed.\n" );
	CS_PrintF( "Thread " + tid.str() + " has completed.\n" );

	return 0;
}

/********************************************************
	getHttpStatusCode
*/
DWORD getHttpStatusCode( HINTERNET &hOpenRequest, const string &siteName, const int threadID )
{
	// Check the HTTP status code
	BOOL queryStatus;
	DWORD statusCode = 0;
	DWORD size = sizeof( DWORD );
	
	// get status code
	queryStatus = HttpQueryInfo( hOpenRequest, HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER, &statusCode, &size, NULL );
	
	if ( !queryStatus )
	{
		DWORD errorCode = GetLastError();
		std::ostringstream stream;
		stream << errorCode;
		string message = "HttpQueryInfo() failed with error code: " + stream.str() + "\n";
		CS_PrintErr( message );
		CS_PrintF( message );
		return 0;
	}

	// print query info
	std::ostringstream stream;
	stream << statusCode;
	string successMsg = "tID " + std::to_string( threadID ) + ' ' + siteName + " HTTP status code: " + stream.str() + "\n";
	CS_Print( successMsg );
	CS_PrintF( successMsg );

	return statusCode;
}

/********************************************************
	CS_Print
*/
void CS_Print( string message )
{
	EnterCriticalSection( &outputCS );
		cout << message << "\n\n";
		cout.flush();
	LeaveCriticalSection( &outputCS );
}

/********************************************************
	CS_PrintErr	
*/
void CS_PrintErr( string message )
{
	EnterCriticalSection( &outputCS );
		cout << message << '\n';
		cout << "GetLastError: " << GetLastError() << "\n\n";
		cout.flush();
	LeaveCriticalSection( &outputCS );
}

/********************************************************
	CS_PrintF
*/
void CS_PrintF( string message )
{
	EnterCriticalSection( &outputCS );
		logFile << getDateTime() << message << '\n';
	LeaveCriticalSection( &outputCS );
}

/********************************************************
	initSiteQueue
*/
HINTERNET initSiteQueue( SiteQueue *queue, SiteSelector *selector, const ThreadInfo &tInfo )
{
	HINTERNET hInstance = NULL;
	HINTERNET hConnect = NULL; 

	// Open internet session
	string proxy = "http=http://127.0.0.1:8123";
	//hInstance = InternetOpen( TEXT( "ProxyTest" ), INTERNET_OPEN_TYPE_PROXY, proxy.c_str(), NULL, INTERNET_NO_CALLBACK );
	hInstance = InternetOpen( TEXT( "ProxyTest" ), INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, INTERNET_NO_CALLBACK );

	if ( NULL == hInstance )
	{
		CS_PrintErr( "Error, InternetOpen failed." );
		CS_PrintF( "Error, InternetOpen failed." );
		return NULL;
	}

	// Create handle to connect to url.
	hConnect = InternetOpenUrl( hInstance, tInfo.siteName.c_str(), NULL, 0, 0, NULL );

	if ( NULL == hConnect )
	{
		CS_PrintErr( "Error, initSiteQueue->InternetOpenUrl failed." );
		CS_PrintF( "Error, initSiteQueue->InternetOpenUrl failed." );
		return NULL;
	}

	getHttpStatusCode( hConnect, tInfo.siteName.c_str(), tInfo.threadID );

	// read HTML
	selector->readHTML( &hConnect );

	queue->fillQueue( selector->getAllSites() );

	return hInstance;
}